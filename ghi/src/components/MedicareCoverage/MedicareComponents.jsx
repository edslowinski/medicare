import React from 'react';

export function PartA() {
	return (
		<div>
			<h1>About Medicare Coverage & Costs</h1>
			<p>
				There are four basic parts of Medicare: A, B, C and D. Each part helps
				to pay for certain health care services. Each part also has certain
				costs that you may have to pay. Your Medicare costs will depend on what
				coverage you choose and on what health care services you use.
			</p>
			<h4>Medicare Part A</h4>
			<p>
				Medicare Part A helps pay for care you receive when you are admitted as
				an inpatient in a hospital or skilled nursing facility.
			</p>
			<p>
				Medicare Part A coverage is hospital insurance. It’s one part (or half)
				of what’s often called Original Medicare, which is administered by the
				federal government. Medicare Part B (medical insurance) is the other
				part.
			</p>
			<p>
				Medicare Part A helps pay for care you receive when you are admitted as
				an inpatient in a hospital or skilled nursing facility. Costs may not be
				covered by Part A if you are in the hospital for observation.
			</p>
			<h5>What Does Medicare Part A Cover?</h5>
			<p>
				Medicare Part A covers the hospital charges and most of the services you
				receive when you’re in the hospital, however it does not cover the fees
				charged by doctors who participate in your care while you’re in the
				hospital. Medicare Part B helps pay those costs. The list below shows
				examples of what Part A covers.
			</p>
			<p>
				<b>
					Medicare Part A covers hospital stays and associated services,
					including:
				</b>
			</p>
			<ol>
				<li>A semi-private room</li>
				<li>Your hospital meals</li>
				<li>Skilled nursing services</li>
				<li>Care on special units, such as intensive care</li>
				<li>
					Drugs, medical supplies and medical equipment used during your
					inpatient stay
				</li>
				<li>Lab tests, X-rays and medical equipment as an inpatient</li>
				<li>Operating room and recovery room services</li>
				<li>
					Some blood transfusions in a hospital or skilled nursing facility
				</li>
				<li>
					Rehabilitation services, including physical therapy received through
					home health care
				</li>
				<li>
					Skilled health care in your homes if you're homebound and only need
					part-time care
				</li>
				<li>
					Care to manage symptoms and control pain for the terminally ill
					(hospice care)
				</li>
			</ol>
			<div>
				<h5>What Does Medicare Part A Cost?</h5>
				<div>
					<p>
						Medicare Part A shares some costs with you if you need to be
						hospitalized. The table below shows the different costs that may
						apply. Costs are shown for 2018.
					</p>
					<p>
						You do not have to pay a premium for Part A if you or your spouse
						worked and paid Medicare taxes for at least 10 years.
					</p>
				</div>
				<div>
					<h5>Premium</h5>
					<div>
						<p>For most people ($0)</p>
					</div>
					<div>
						<p>If applicable (Up to $437 per month)</p>
					</div>
				</div>
				<div>
					<h5>Deductible</h5>
					<div>
						<p>Per benefit period ($1,364)</p>
					</div>
				</div>
				<div>
					<h5>Hospital Co-payment</h5>
					<p>Days 1-60 ($0)</p>
					<p>Days 61-90 ($341 per day)</p>
					<p>
						Days 91 and beyond ($682 per day up to 60 *lifetime reserve days)
					</p>
					<p>
						*Lifetime reserve days: In Medicare Part A, a set number of covered
						hospital days you can draw on if you are in the hospital longer than
						90 days in a benefit period. You have 60 lifetime reserve days. A
						lifetime reserve day cannot be replaced. When it is used up, it is
						gone.
					</p>
				</div>
				<div>
					<h5>Skilled Nursing Facility Co-payment</h5>
					<p>Days 61-90 ($341 per day)</p>
					<p>Days 1-20 ($0)</p>
					<p>Days 21-100 (Up to $170.50 per day)</p>
				</div>
				<div>
					<h5>Hospice</h5>
					<p>
						Medications for pain and symptom management (Up to $5 per
						prescription) Durable medical equipment used at home (20% of the
						cost) Respite care (5% of the *Medicare-approved amount)
					</p>
					<p>
						*Medicare-approved amount: The amount Medicare determines to be
						reasonable for a covered service. Providers who “accept assignment”
						agree to accept this amount as payment in full. Providers who accept
						Medicare but not assignment can charge up to 15% above this amount.
					</p>
				</div>
				<div>
					<h5>How Medicare Part A Cost Sharing Works</h5>
					<p>
						Medicare Part A pays most of the hospital costs for stays up to 60
						days. But if you have a very long stay, you could be responsible for
						a large share of the cost.
					</p>
				</div>
			</div>
		</div>
	);
}

export function PartB() {
	return (
		<div>
			<h4>Medicare Part B</h4>
			<p>
				Medicare Part B is medical insurance. It’s one part of what’s often
				called Original Medicare, which is administered by the federal
				government. Medicare Part A (hospital insurance) is the other
				part.Medicare Part B helps pay for care you receive in a clinic or
				hospital as an outpatient. Part B also covers most doctor services you
				receive as a hospital inpatient. Most other hospital services are
				covered by Part A.
			</p>
			<h5>What Does Medicare Part B Cover?</h5>
			<p>
				Medicare Part B covers doctor visits and most routine and emergency
				medical services. It also covers some preventive care, like flu shots.
				The list below shows more examples of what Part B covers.
			</p>
			<p>
				<b>
					Medicare Part B covers doctor and outpatient visits and associated
					services, including:
				</b>
			</p>
			<ol>
				<li>Doctor visits (including an annual Wellness Visit)</li>
				<li>Ambulatory surgery center services</li>
				<li>Outpatient medical services</li>
				<li>Some preventive care, like flu shots</li>
				<li>Clinical laboratory services, like blood and urine tests</li>
				<li>X-rays, MRIs, CT scans, EKGs and some other diagnostic tests</li>
				<li>
					Durable medical equipment for use at home, like wheelchairs and
					walkers
				</li>
				<li>Emergency room services</li>
				<li>
					Skilled nursing care and health aide services for the homebound on a
					part-time or intermittent basis
				</li>
				<li>Mental health care as an outpatient</li>
			</ol>
			<h5>What Does Medicare Part B Cost?</h5>
			<p>
				Medicare Part B shares some costs with you when you see the doctor or
				use other medical services. The table below shows the different costs
				that may apply. Costs shown are for 2019.
			</p>
			<p>
				Part B charges a monthly premium. The payment is deducted from your
				monthly check if you receive Social Security benefits. Otherwise you
				need to send a monthly premium payment to Medicare.
			</p>
			<div>
				<h5>Premium</h5>
				<p>Per month ($135.50 to $460.50 depending on income)</p>
				<h5>Deductible</h5>
				<p>Per year ($185)</p>
				<h5>Co-insurance</h5>
				<p>Most medical services (20% of the *Medicare-approved amount)</p>
				<p>
					*Medicare-approved amount: The amount Medicare determines to be
					reasonable for a covered service. Providers who “accept assignment”
					agree to accept this amount as payment in full. Providers who accept
					Medicare but not assignment can charge up to 15% above this amount.
				</p>
				<p>
					Durable medical equipment (20% of the cost Medicare-approved amount)
				</p>
				<p>
					Outpatient mental health care (20% of the Medicare-approved amount)
				</p>
				<h5>How Medicare Part B Cost Sharing Works</h5>
				<p>
					Medicare Part B pays 80% of the cost for most outpatient care and
					services, and you pay 20%. But there is something called “Medicare
					assignment” that’s important to understand.
				</p>
				<p>
					Doctors and providers who accept Medicare assignment agree to take
					what Medicare pays—the Medicare-approved amount—as payment in full.
					Medicare reduces the approved amount it pays for doctors who don’t
					accept Medicare assignment. Doctors who don’t accept Medicare
					assignment may charge more than the Medicare-approved amount. You may
					have to pay the additional cost, which is called “excess charges.”
				</p>
			</div>
		</div>
	);
}

export function PartC() {
	return (
		<div>
			<h4>Medicare Part C</h4>
			<p>
				Medicare Part C is also called Medicare Advantage. It’s an alternative
				to Original Medicare (Parts A and B).
			</p>
			<p>
				Medicare Advantage plans are offered by private insurance companies
				approved by Medicare. You must be enrolled in both Part A and Part B to
				join a Medicare Advantage plan. You’ll still be in the Medicare program,
				but you will receive your benefits through the plan instead of through
				Original Medicare.
			</p>
			<h5>What Does Medicare Part C Cover?</h5>
			<p>
				Medicare Advantage (Part C) plans combine coverage for hospital care,
				doctor visits and other medical services all in one plan. Plans are
				required to provide all of the benefits offered by Medicare Parts A and
				B (except hospice care, which continues to be provided by Part A). Many
				plans also provide prescription drug coverage and additional benefits
				like routine dental and eye care.
			</p>
			<h5>What do Medicare Advantage (Part C) plans cover?</h5>
			<ul>
				<li>
					Medicare Advantage (Part C) includes all the benefits of Part A,
					including hospital stays, skilled nursing, home health care and
					associated services
				</li>
				<li>
					Medicare Advantage (Part C) includes all the benefits of Part B,
					including doctors visits, outpatient care, screening and lab tests and
					associated services
				</li>
				<li>
					Prescription drug coverage is included in many Medicare Advantage
					plans
				</li>
				<li>
					Additional benefits may be included, such as dental eye care, hearing
					care, wellness services and a nurse helpline
				</li>
				<li>
					Medicare Advantage plans all have a yearly limit on your out-of-pocket
					costs for covered medical services. This limit may vary for different
					Medicare Advantage plans and can change each year
				</li>
			</ul>
			<h5>What Does Medicare Part C Cost?</h5>
			<p>
				Each Medicare Advantage (Part C) plan sets its own specific costs, but
				the types of costs they include are similar. The table below shows the
				types of costs that plans may apply, but you need to look at the details
				of a particular plan for actual costs.
			</p>
			<div>
				<h5>Premium</h5>
				<p>
					Per month (Plan premiums vary. You still pay the Part B premium to
					Medicare and the Part A premium, if you have one).
				</p>
				<h5>Deductible</h5>
				<p>
					Per year (Some plans charge an annual deductible, and some don’t. Part
					A and B deductibles do not apply).
				</p>
				<h5>Co-Payment</h5>
				<p>
					Most medical services, such as doctor visits Many plans charge co-pays
					for the services and benefits you use.
				</p>
				<h5>Co-insurance</h5>
				<p>
					Select items, such as durable medical equipment (Plans set their own
					co-insurance terms and percentages).
				</p>
				<h5>How Medicare Advantage Cost Sharing Works</h5>
				<p>
					Most Medicare Advantage plans use a combination of deductibles,
					coinsurance and copays to share the cost of the services you use.
					Cost-sharing usually applies to all of the services the plan covers.
				</p>
				<p>
					You need to read the details of each individual Medicare Advantage
					plan to get the full story on its costs. Most plans have network
					doctors and pharmacies that may offer plan members discounted pricing.
				</p>
				<p>
					The following stories may help you understand how cost sharing might
					work with a Medicare Advantage plan in different situations.
				</p>
			</div>
		</div>
	);
}

export function PartD() {
	return (
		<div>
			<h4>Medicare Part D</h4>
			<p>
				Medicare Part D is prescription drug coverage. It helps pay for the
				medications your doctor prescribes.
			</p>
			<p>
				Original Medicare (Parts A and B) does not cover prescription drugs.
				Many people who choose Original Medicare add a prescription drug (Part
				D) plan or choose a Medicare Advantage plan that includes Part D.
			</p>
			<p>
				In general, you may enroll in a Part D plan if you are entitled to
				Medicare Part A or if you are enrolled in Medicare Part B. In addition,
				you must live in the service area of a Part D plan.
			</p>
			<h5>What Does Medicare Part D Cover?</h5>
			<p>
				Medicare Part D Plans are required to cover certain common types of
				drugs, but each plan may choose which specific drugs it covers. The
				drugs you take may not be covered by every Part D plan. You need to
				review each plan’s drug list, or formulary, to see if your drugs are
				covered.
			</p>
			<p>Medicare Part D covers certain prescription drugs</p>
			<ul>
				<li>
					The federal government sets guidelines for the types of drugs Part D
					plans must cover
				</li>
				<li>
					Each Part D plan decides which specific drugs it will cover and what
					premium members will pay
				</li>
				<li>
					When comparing Part D coverage, check each plan's formulary (drug
					list) to make sure your drugs are included.
				</li>
			</ul>
			<h5>What Does Medicare Part D Cost?</h5>
			<p>
				The insurance companies that offer Medicare Part D drug plans and
				Medicare Advantage (Part C) plans with drug coverage set their own
				prices, but the types of costs they include are similar. The table below
				shows the types of costs that plans may apply. Part D plan premiums and
				cost sharing can vary widely, even for similar coverage. You need to
				review plan details carefully.
			</p>
			<div>
				<h5>Premium</h5>
				<p>
					Per month (Plan premiums vary. You still pay the Part B premium to
					Medicare and the Part A premium, if you have one).
				</p>
				<h5>Deductible</h5>
				<p>
					Per year (Some plans charge an annual deductible, and some don’t).
				</p>
				<h5>Co-Payment</h5>
				<p>
					Most new prescriptions and refills (Some plans charge co-pay each time
					you fill a prescription).
				</p>
				<h5>Co-insurance</h5>
				<p>
					Some new prescriptions and refills (Some plans charge a percentage of
					the cost when you fill a prescription).
				</p>
				<h5>How Medicare Part D Cost Sharing Works</h5>
				<p>
					Medicare Part D has different stages of cost sharing until you reach a
					set limit on out-of-pocket costs for the year. The limit is $5,100 in
					2019. After that, your plan pays most of the cost of your drugs for
					the rest of the year.
				</p>
				<p>
					Co-pays, co-insurance amounts and your plan deductible, if any, count
					as out-of-pocket costs. Premium payments do not.
				</p>
				<p>
					Part D cost-sharing stages are explained below. The costs shown are
					for 2019. You may not go through every stage in any given year. If you
					get Extra Help (link to Get Help Paying for Medicare page) from
					Medicare for Part D costs, the coverage gap stage doesn’t apply to
					you.
				</p>
			</div>
		</div>
	);
}

export function AdditionalInfo() {
	return (
		<div>
			<h4>Medicare Costs To Understand</h4>
			<p>
				Medicare helps pay for many health care items and services, but you will
				have to pay a share of the cost, too. Your Medicare costs include:
			</p>
			<ul>
				<li>Premiums</li>
				<li>Deductibles</li>
				<li>Co-payments</li>
				<li>Co-insurance</li>
			</ul>
			<p>
				You pay premiums outright. Deductibles, co-payments (co-pays) and
				co-insurance are ways that Medicare shares the cost of your care with
				you.
			</p>
			<h5>Premium</h5>
			<p>A premium is a fixed amount that you pay for the cost of the plan.</p>
			<h5>Deductible</h5>
			<p>
				A deductible is the out-of-pocket cost due to you prior to your plan
				paying any cost for health care services.
			</p>
			<h5>Co-Payment</h5>
			<p>
				A co-payment, or co-pay, is a fixed amount you pay at the time you
				receive a covered service.
			</p>
			<h5>Co-insurance</h5>
			<p>
				Co-insurance is when you and your plan split the cost of a covered
				service. For example, you might pay 20% of the allowed amount and your
				plan would pay 80%.
			</p>
			<h5>Out-of-Pocket Maximum</h5>
			<p>
				The out-of-pocket maximum is the total amount you might pay during a
				calendar year. The total does not include your premium or the cost of
				any services that are not covered by your plan. After you reach your
				out-of-pocket maximum, your plan pays 100% of the allowed amount for
				covered services for the rest of the year.
			</p>
		</div>
	);
}

export function MedicareMedicaidInfo() {
	return (
		<div className="m-6">
			<h2 className="text-3xl font-bold mb-4">
				Medicare & Medicaid Working Together
			</h2>
			<p className="mb-4">
				Medicare and Medicaid might sound and act similarly, but in fact they
				are vastly different. While Medicare is the federal health care program
				for typically aged individuals and those on disability, Medicaid is for
				the population on a very low income. Medicaid is the federal and state
				program that provides health coverage to those who cannot afford it for
				themselves. When someone who lives on a very low income goes onto
				Medicare, they become dual eligible for both programs, at the same time.
				There are special plans that have been created specifically to meet the
				needs of people who receive both Medicare and Medicaid!
			</p>
			<div className="overflow-x-auto">
				<table className="min-w-full divide-y divide-gray-200">
					<thead>
						<tr>
							<th className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
								Medicare
							</th>
							<th className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
								Medicaid
							</th>
						</tr>
					</thead>
					<tbody className="bg-white divide-y divide-gray-200">
						<tr>
							<td className="px-6 py-4">
								What is it? A federal health insurance program for people who
								are: 65 or older, under 65 with certain disabilities, of any age
								and have End Stage Renal Disease (ESRD) or ALS
							</td>
							<td className="px-6 py-4">
								What is it? A joint federal and state program that helps pay
								health care costs for certain people and families with limited
								income and resources. Different programs under the Medicaid
								umbrella are designed to help specific populations.
							</td>
						</tr>
						<tr>
							<td className="px-6 py-4">Who governs it? Federal government</td>
							<td className="px-6 py-4">Who governs it? State governments</td>
						</tr>
						<tr>
							<td className="px-6 py-4">
								What does it cover? Depends on the coverage you choose and may
								include: care and services received as an inpatient in a
								hospital or skilled nursing facility (Part A), doctor visits,
								care and services received as an outpatient, and some preventive
								care (Part B), prescription drugs (Part D). Note: Medicare
								Advantage plans (Part C) combine Part A and Part B coverage, and
								often include drug coverage (Part D) as well - all in one plan.
							</td>
							<td className="px-6 py-4">
								What does it cover? Every state has its own Medicaid programs,
								and so the coverage varies, dependent upon where you reside, but
								there are federal guidelines that require certain benefits be
								mandatory. Many states also opt in to offering additional and
								optional benefits as well. Care and services received in a
								hospital or skilled nursing facility, care and services received
								in a federally-qualified health centers, rural health clinic or
								freestanding birth center (licensed or recognized by your
								state), doctor, nurse midwife, and certified pediatric and
								family nurse practitioner services and more.
							</td>
						</tr>
						<tr>
							<td className="px-6 py-4">
								What does it cost? It depends on the coverage you choose. Costs
								may include premiums, deductibles, copays and coinsurance.
							</td>
							<td className="px-6 py-4">
								What does it cost? It depends on your income and the rules in
								your state. Costs may include premiums, deductibles, copays and
								coinsurance. Certain groups are exempt from most out-of-pocket
								costs.
							</td>
						</tr>
						<tr>
							<td className="px-6 py-4">
								How do I get it? Many people are enrolled in Parts A and B
								automatically when they turn 65. You can also contact your local
								Social Security office to see if you are eligible.
							</td>
							<td className="px-6 py-4 ">
								How do I get it? Since each state governs its own Medicaid
								programs, the eligibility to get onto Medicaid varies. Most
								states do offer an online electronic application process for
								applying for Medicaid and have phone lines you can call to speak
								with a live person about Medicaid and how you may qualify.
							</td>
						</tr>
						<tr>
							<td></td>
							<td className="px-6 py-4 ">
								How does my Medicaid work with my Medicare? There are different
								levels of Medicaid, which provide varying levels of financial
								assistance for your health care. Speak to your local Essential
								Solutions Medicare representative to learn what level of
								Medicaid you receive and how your specific Medicaid level
								financially assists with your Medicare health care costs.
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
}

export function MedicalEligibility() {
	return (
		<div className="m-6">
			<h2 className="text-3xl font-bold mb-4">Medicare Eligibility</h2>
			<p className="mb-4">
				Medicare is available to United States citizens and legal residents who
				have lived in the United States for at least 5 years in a row. Medicare
				is individual insurance. It doesn't cover spouses or dependents. You may
				become eligible to receive Medicare benefits based on any one of the
				following.
			</p>
			<h3 className="text-2xl font-bold mb-2">
				I am Turning Age 65 or am Older than Age 65
			</h3>
			<p className="mb-4">
				Did you know that on average 10,000 Baby Boomers are turning Age 65
				every day for the next decade? Below are some important facts you need
				to know to prepare for your Medicare.
			</p>
			<ul className="mb-4">
				<li>
					You must be 65 to enroll in Medicare—your spouse’s age doesn’t count.
				</li>
				<li>
					You may enroll in Medicare even if you’re not collecting Social
					Security income yet.
				</li>
				<li>
					You may enroll in Medicare even if you work past age 65 and have
					employer coverage, or you are 65 and have coverage through your
					spouse’s employer.
				</li>
			</ul>
			<h3 className="text-2xl font-bold mb-2">When to Enroll in Medicare</h3>
			<p className="mb-4">
				The Initial Enrollment Period (IEP) is your first opportunity to enroll
				in Medicare. The IEP begins three months before, the month, and three
				months after your 65th birthday. This equates to a total 7 month period
				of time that you have to enroll into Medicare. You have the right to
				enroll during your IEP or at a later date, however you should be aware
				of how deferring your Medicare enrollment can affect you.
			</p>
			<h3 className="text-2xl font-bold mb-2">Medicare Enrollment</h3>
			<p className="mb-4">
				You can enroll in person at your nearest Social Security office or you
				can enroll easily online!
				<a
					href="https://secure.ssa.gov/iClaim/rib"
					target="_blank"
					rel="noopener noreferrer"
				>
					Direct link to online Medicare Enrollment
				</a>
			</p>
			<h3 className="text-2xl font-bold mb-2">Deferring Medicare</h3>
			<p className="mb-4">
				Some people do not choose to begin Medicare at the age of 65 and instead
				elect to defer to a later date. You can easily do this by simply not
				enrolling into Medicare Part B. Remaining with your current health
				provider, either through work or your spouse's work qualifies you to
				defer taking Medicare on yourself.
			</p>
			<h3 className="text-2xl font-bold mb-2">
				Deferred Medicare and Ready to Retire?
			</h3>
			<p className="mb-4">
				Once you are ready to enroll into Medicare you will need to apply for
				Part B. You can do this online or at your local Social Security office.
				The date your Part B (medical insurance) begins always falls on the
				first of the month. You will need to make sure you receive, from the
				current health insurance provider, a Letter of Voluntary Disenrollment.
				Provide this letter to your designated Medicare Advisor so that when
				applying for Medicare Advantage or a Supplement plan, the insurance
				carrier understands that you are within a Special Election Period (SEP)
				and not subject to medical underwriting. This will also ensure that
				Centers for Medicare Services (CMS) and the Social Security
				Administration (SSA) do not hit you with a Late Enrollment penalty.
			</p>
			<p className="mb-4">
				Beware of enrolling into Medicare Part B and electing to stay with your
				employer at the same time. It is not the same as deferring your
				Medicare, and if you desire at some point into the future to enroll in a
				Supplement plan, you could be subjected to medical underwriting. If your
				health at that point in the future is not a risk that the insurance
				carrier is willing to assume your plan could be denied, preventing you
				from ever obtaining a medical Supplement.
			</p>
		</div>
	);
}
