import React from 'react';

const Footer = () => {
    return (
        <footer className="bg-[#203260] text-white py-4">
        <div className="container mx-auto flex flex-col md:flex-row justify-center items-center">
            <p className="text-center">&copy; 2023 All Rights Reserved Essential Solutions. Website Design & Developed by <a href="https://essentialsolutionsusa.com/home" target='_blank' className="text-blue-500 hover:underline">Essential Solutions</a>.</p>
        </div>
        </footer>
    );
};

export default Footer;
