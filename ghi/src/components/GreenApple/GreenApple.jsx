import React from 'react';
import seed from './images/seed.png';
import budding from './images/budding.png';
import FullTree from './images/FullTree.png';
import banner from './images/applebanner.png';

function CHCA() {
	// find an advisor page at end
	return (
		<>
			<div>
				<section
					className="bg-gray-100 py-10"
					style={{ backgroundImage: `url(${banner})` }}
				>
					<div className="container mx-auto px-4">
						<div className="max-w-4xl mx-auto">
							<h1
								className="text-4xl font-bold leading-tight text-gray-900"
								style={{ textAlign: 'center' }}
							>
								Certified Health Care Advisor
							</h1>
						</div>
					</div>
				</section>
				<section className="bg-white">
					<div className="container mx-auto py-16 px-4">
						<div className="max-w-4xl mx-auto">
							<h1 className="text-3xl font-bold leading-tight mb-8 text-medicalblue">
								The Green Apple Difference
							</h1>
							<div className="text-gray-700">
								<p className="mb-4">
									The CHCA designation is the standard that we are setting at
									Essential Solutions for our advisor member services. This is
									the standard that we expect all our advisors to want to
									achieve and deliver to each and every member. To achieve this
									certification, they must complete a course and obtain a 90% or
									greater on the final examination. They are also required to
									complete an annual recertification each year.
								</p>
								<p>
									To be a Certified Health Care Advisor - CHCA is a standard
									that we hope changes the industry. We want each of our members
									to know that if they see this designation, they have completed
									extensive training making them an advisor above the rest. That
									training certifies they are doing a complete assessment of
									each member and enrolling them in the plan that best fits
									their needs. Not just the enrollment process but the customer
									care service that comes after they have enrolled a member as
									well. Keeping our Members First.
								</p>
							</div>
							<div className="container mx-auto py-16">
								<div className="max-w-4xl mx-auto">
									<h1 className="text-3xl font-bold leading-tight text-medicalblue mb-8">
										Our Philosophy
									</h1>
									<div className="text-gray-700">
										<div className="relative mb-4">
											<img src={seed} alt="Seed" />
											<p className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center text-white font-bold text-xl bg-black bg-opacity-50 p-4">
												All your efforts began from a simple seed and from that
												seed a mighty tree grew
											</p>
										</div>
										<div className="relative mb-4">
											<img src={budding} alt="Budding" />
											<p className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center text-white font-bold text-xl bg-black bg-opacity-50 p-4">
												A tree began to bud and then flower
											</p>
										</div>
										<div className="relative mb-4">
											<img src={FullTree} alt="Budding" />
											<p className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center text-white font-bold text-xl bg-black bg-opacity-50 p-4">
												And finally, a juicy ripe fruit for the picking!
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</>
	);
}

export default CHCA;
