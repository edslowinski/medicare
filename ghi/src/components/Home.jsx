import React from 'react'

const Home = () => {
    return (
        <div className="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
            <div className="relative py-3 sm:max-w-xl sm:mx-auto">
                <div className="absolute inset-0 bg-gradient-to-r from-green-400 to-blue-500 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
                <div className="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
                    <h1 className="text-2xl md:text-3xl lg:text-4xl leading-tight font-extrabold text-gray-900 tracking-tight sm:text-5xl md:text-6xl">Welcome to Essential Solutions</h1>
                    <h2 className="mt-8 text-xl text-gray-500">Your trusted Medicare partner</h2>

                    <p className="mt-5 text-lg text-gray-500">
                        Essential Solutions is here to ensure your medical needs are met with the highest quality of care. We're located at 34 Patrick Lane, Depew NY 14043.
                    </p>

                    <div className="mt-10">
                        <a href="#" className="py-3 px-6 text-white bg-indigo-600 rounded-md">Schedule an Appointment</a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
