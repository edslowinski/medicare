import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
	const links = [
		{ name: 'Home', path: '/home' },
		{ name: 'Our values', path: '/GreenAppleDifference' },
		{ name: 'Services', path: '/homeee' },
		{ name: 'Contact', path: '/contact' },
	];

	const [isDropdownOpen, setIsDropdownOpen] = useState(false);

	const toggleDropdown = () => {
		setIsDropdownOpen(!isDropdownOpen);
	};

	return (
		<nav className="flex items-center justify-between p-4 bg-gray-800 text-white">
			<div className="flex items-center">
				<img
					src="placeholder-image.jpg" // Replace with your actual image source
					alt="Company Logo"
					className="h-8 mr-2"
				/>
				<h1 className="text-xl font-bold">Essential Solutions</h1>
			</div>
			<div className="hidden md:flex">
				{links.map((link) => (
					<Link
						key={link.path}
						to={link.path}
						className="ml-4 hover:text-gray-300"
					>
						{link.name}
					</Link>
				))}
			</div>
			<div className="md:hidden">
				<button
					className="ml-4 px-4 py-2 rounded bg-blue-500 text-white"
					onClick={toggleDropdown}
				>
					Menu
				</button>
				{isDropdownOpen && (
					<div className="absolute right-0 mt-2 py-2 bg-white text-gray-800">
						{links.map((link) => (
							<Link
								key={link.path}
								to={link.path}
								className="block px-4 py-2 hover:bg-gray-200"
							>
								{link.name}
							</Link>
						))}
					</div>
				)}
			</div>
		</nav>
	);
};

export default Navbar;
