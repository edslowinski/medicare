import React from 'react';
import Andrew from './images/Andrew.jpg';
import Greg from './images/Greg.jpg';
import Jeff from './images/Jeff.jpg';
import {
	DeviceMobileIcon,
	LocationMarkerIcon,
	MailIcon,
} from '@heroicons/react/outline';

const ContactUs = () => {
	return (
		<div className="relative mx-auto py-10 px-4 w-full max-w-7xl bg-gray-50 pt-20">
			<div className="mx-auto max-w-5xl">
				<div>
					<span className="absolute top-0 left-0 hidden md:block opacity-10">
						<svg
							width={250}
							height={600}
							fill="none"
							viewBox="0 0 250 600"
							aria-hidden="true"
						>
							<pattern
								id="pattern-rectangles"
								x={0}
								y={0}
								width={40}
								height={40}
								patternUnits="userSpaceOnUse"
							>
								<rect
									x={0}
									y={0}
									width={10}
									height={10}
									className="text-green-500"
									fill="currentColor"
								/>
							</pattern>
							<rect width={250} height={600} fill="url(#pattern-rectangles)" />
						</svg>
					</span>
					<span className="absolute bottom-0 right-0 opacity-20">
						<svg
							width={300}
							height={300}
							fill="none"
							viewBox="0 0 300 300"
							aria-hidden="true"
						>
							<pattern
								id="pattern-circles"
								x="0"
								y="0"
								width="30"
								height="30"
								patternUnits="userSpaceOnUse"
								patternContentUnits="userSpaceOnUse"
							>
								<circle
									id="pattern-circle"
									cx="10"
									cy="10"
									r="5"
									className="fill-current text-green-500"
								/>
							</pattern>
							<rect
								id="rect"
								x="0"
								y="0"
								width="100%"
								height="100%"
								fill="url(#pattern-circles)"
							/>
						</svg>
					</span>
				</div>

				<div className="relative space-y-5">
					<h2 className="text-center text-5xl text-green-500 font-light">
						Contact Us
					</h2>
					<p className="mx-auto py-5 max-w-3xl text-center text-base text-gray-600">
						Essential Health Care. Get Scammed by old people for old people.
					</p>
					<div className="flex flex-wrap justify-between items-center text-base">
						<div className="m-2.5 inline-flex items-center">
							<LocationMarkerIcon className="mr-2 w-6 h-6 text-green-500" />
							<p className="text-gray-600 font-semibold">
								34 Patrick Lane, Depew NY 14043
							</p>
						</div>
						<div className="m-2.5 inline-flex items-center">
							<DeviceMobileIcon className="mr-2 w-6 h-6 text-green-500" />
							<p className="text-gray-600 font-semibold">+1 (716) 400-7412</p>
						</div>
						<div className="m-2.5 inline-flex items-center">
							<MailIcon className="mr-2 w-6 h-6 text-green-500" />
							<p className="text-gray-600 font-semibold">
								Edward.essentialsolutions@gmail.com
							</p>
						</div>
					</div>
				</div>
				<div className="relative mt-16 rounded border-2 border-gray-200">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2921.781028645962!2d-78.73511692353665!3d42.919658899564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d30b4f1baf4b23%3A0x4ae9ae85332796ab!2s34%20Patrick%20Ln%2C%20Depew%2C%20NY%2014043!5e0!3m2!1sen!2sus!4v1689019315190!5m2!1sen!2sus"
						title="map"
						scrolling="no"
						frameBorder="0"
						width="100%"
						height="450"
						className=""
						loading="lazy"
					/>
				</div>
				<div className="mt-16">
					<h3 className="text-center text-3xl text-green-500 font-light mb-5">
						Meet the Team
					</h3>
					<div className="flex justify-center">
						<a
							href="https://www.linkedin.com/in/chris-zambito"
							target="_blank"
							rel="noopener noreferrer"
							className="m-2"
						>
							<div className="w-32 h-32 bg-gray-200 rounded-full overflow-hidden">
								<img
									src="https://media.licdn.com/dms/image/C5603AQHapM39qgbg4w/profile-displayphoto-shrink_200_200/0/1517549318637?e=1694649600&v=beta&t=AklcUSBD5BP467cTdqb-H6lkihvU8_UpIQ0w6QVYMBo"
									alt="Dev 1"
									className="w-full h-full object-cover"
								/>
							</div>
							<p className="text-center mt-3 text-gray-600 font-semibold">
								Ed Slowinski
							</p>
						</a>
						<a
							href="https://www.linkedin.com/in/edward-slowinski-0a416bb1/"
							target="_blank"
							rel="noopener noreferrer"
							className="m-2"
						>
							<div className="w-32 h-32 bg-gray-200 rounded-full overflow-hidden">
								<img
									src={Greg}
									alt="Dev 2"
									className="w-full h-full object-cover"
								/>
							</div>
							<p className="text-center mt-3 text-gray-600 font-semibold">
								Greg Solazzo
							</p>
						</a>
						<a
							href="https://www.linkedin.com/in/edward-slowinski-0a416bb1//"
							target="_blank"
							rel="noopener noreferrer"
							className="m-2"
						>
							<div className="w-32 h-32 bg-gray-200 rounded-full overflow-hidden">
								<img
									src={Andrew}
									alt="Dev 3"
									className="w-full h-full object-cover"
								/>
							</div>
							<p className="text-center mt-3 text-gray-600 font-semibold">
								Andrew Morel
							</p>
						</a>
						<a
							href="https://www.linkedin.com/in/edward-slowinski-0a416bb1/"
							target="_blank"
							rel="noopener noreferrer"
							className="m-2"
						>
							<div className="w-32 h-32 bg-gray-200 rounded-full overflow-hidden">
								<img
									src={Jeff}
									alt="Dev 4"
									className="w-full h-full object-cover"
								/>
							</div>
							<p className="text-center mt-3 text-gray-600 font-semibold">
								Jeff Young
							</p>
						</a>
						<a
							href="https://www.linkedin.com/in/edward-slowinski-0a416bb1/"
							target="_blank"
							rel="noopener noreferrer"
							className="m-2"
						>
							<div className="w-32 h-32 bg-gray-200 rounded-full overflow-hidden">
								<img
									src="https://media.licdn.com/dms/image/C5603AQHapM39qgbg4w/profile-displayphoto-shrink_200_200/0/1517549318637?e=1694649600&v=beta&t=AklcUSBD5BP467cTdqb-H6lkihvU8_UpIQ0w6QVYMBo"
									alt="Dev 4"
									className="w-full h-full object-cover"
								/>
							</div>
							<p className="text-center mt-3 text-gray-600 font-semibold">
								Jackie Manzella
							</p>
						</a>
						<a
							href="https://www.linkedin.com/in/edward-slowinski-0a416bb1/"
							target="_blank"
							rel="noopener noreferrer"
							className="m-2"
						>
							<div className="w-32 h-32 bg-gray-200 rounded-full overflow-hidden">
								<img
									src="https://media.licdn.com/dms/image/C5603AQHapM39qgbg4w/profile-displayphoto-shrink_200_200/0/1517549318637?e=1694649600&v=beta&t=AklcUSBD5BP467cTdqb-H6lkihvU8_UpIQ0w6QVYMBo"
									alt="Dev 4"
									className="w-full h-full object-cover"
								/>
							</div>
							<p className="text-center mt-3 text-gray-600 font-semibold">
								Jeff Young
							</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ContactUs;
