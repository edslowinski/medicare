import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './components/Home';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import ContactUs from './components/contact/ContactUs';
import CHCA from './components/GreenApple/GreenApple';
import {
	PartA,
	PartB,
	PartC,
	PartD,
	AdditionalInfo,
	MedicalEligibility,
	MedicareMedicaidInfo,
} from './components/MedicareCoverage/MedicareComponents';

function App() {
	return (
		<BrowserRouter>
			<Navbar />
			<>
				<Routes>
					<Route path="/home" element={<Home />} />
					<Route path="/contact" element={<ContactUs />} />
					<Route path="/GreenAppleDifference" element={<CHCA />} />
					<Route path="/InsuranceInfo" element={<MedicareMedicaidInfo />} />
					<Route path="/MedicalEligibility" element={<MedicalEligibility />} />
					<Route path="/PartA" element={<PartA />} />
					<Route path="/PartB" element={<PartB />} />
					<Route path="/PartC" element={<PartC />} />
					<Route path="/PartD" element={<PartD />} />
					<Route path="/AdditionalInfo" element={<AdditionalInfo />} />
				</Routes>
			</>
			<Footer />
		</BrowserRouter>
	);
}

export default App;
